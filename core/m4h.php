<?php
ini_set('display_errors', 'On');
require_once(ABSPATH . WPINC . '/pluggable.php');

add_action('admin_menu','m4h_admin_menu');
//scripts & stylesheets
add_action('init','tern_wp_members_styles');
add_action('init','tern_wp_members_js');
add_action('wp_print_scripts','tern_wp_members_js_root');
//hide new members
add_action('user_register','tern_wp_members_hide');
//short code
add_shortcode('members-list','tern_wp_members_shortcode');
//errors
add_action('init','WP_members_list_errors');

global $getMaps;
if( $_POST['m4h-add-user'] ) {
  $fname = sanitize_text_field( $_POST['first_name'] ); 
  $lname = sanitize_text_field( $_POST['last_name'] ); 
  $email = sanitize_email( $_POST['email'] );
  $line1 = sanitize_text_field( $_POST['line1'] );
  $line2 = sanitize_text_field( $_POST['line2'] );
  $city = sanitize_text_field( $_POST['city'] );
  $state = sanitize_text_field( $_POST['state'] );
  $zip = sanitize_text_field( $_POST['zip'] );

  $addrKeys = array( 'line1', 'line2', 'city', 'state', 'zip' );
  foreach( $addrKeys as $addrKey ) {
    $address[$addrKey] =  $_POST[$addrKey];
  }

  $geocode = $getMap->geoLocate($address);
  $username = strtolower( substr($fname, 0, 1) . $lname );

  // create new user
  $userdata = array(
    'ID' => '',
    'user_pass' => wp_generate_password(),
    'user_login' => $username,
    'user_email' => $email,
    'first_name' => $fname,
    'last_name' => $lname,
    'role' => get_option('default_role')
  );

  $user_id = wp_insert_user( $userdata );
  
  // add tern meta data
  foreach($addrKeys as $addrKey) {
    update_user_meta($user_id, '_' . $addrKey, sanitize_text_field($_POST[$addrKey]) );
  }

  // add lat/long meta data
  update_user_meta($user_id, '_lat', $geocode['lat']);
  update_user_meta($user_id, '_lng', $geocode['lng']);
}

function m4h_admin_menu() {
  if( function_exists('add_menu_page') ) {
    add_menu_page( 'M4H Directory', 'M4H Directory', 'administrator', __FILE__, 'm4h_directory_settings' );
    add_submenu_page( __FILE__, 'Bulk Import', 'M4H Bulk Import', 'administrator', 'm4h-bulk-import', 'm4h_bulk_import' );
  }
}
?>
