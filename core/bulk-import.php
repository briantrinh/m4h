<?php
if ($_REQUEST['page'] !== 'm4h-bulk-import') {
  return;
}

add_action('init','WP_members_list_settings_actions');
add_action('init','WP_members_list_settings_styles');
add_action('init','WP_members_list_settings_scripts');

if ( isset($_POST['m4h-bulk-insert']) ) {
  if ( isset($_FILES['m4h-csv-import']) ) {
    if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );

    $file = $_FILES['m4h-csv-import'];
    $upload = wp_handle_upload($file,array('test_form' => false, 'mimes' => array('csv' => 'text/csv') ) );

    if(!isset($upload['error']) && isset($upload['file'])) {
      // do CSV parsing
      if (isset( $_POST['csv-header'] ) ) {
        parse_csv( $upload['file'], $_POST['csv-header'] );
      }
      else {
        parse_csv( $upload['file'], false );
      }

      // add success message
    }
    else {
      echo "Return Code: " . $file["error"] . "<br />";
    }
  }
  else {
    echo "No file selected. <br/>";
  }
}

function parse_csv($fileHandle, $hasHeaders) {
  global $getMap;
  $f = fopen($fileHandle, 'r');
  if(isset($hasHeaders) && $hasHeaders) fgets($f);

  if($f) {
    //$i = ($hasHeaders ? 1 : 0);
    while ($line = fgetcsv($f, 5000, ",") ) {
      $username = strtolower( substr($line[0], 0, 1) . $line[1] );

      $userdata = array(
        'ID' => '',
        'user_pass' => wp_generate_password(),
        'user_login' => $username,
        'user_email' => $line[2],
        'first_name' => $line[0],
        'last_name' => $line[1],
        'role' => get_option('default_role')
      );

      $location = array(
        'line1' => $line[3],
        'line2' => $line[4],
        'city' => $line[5],
        'state' => $line[6],
        'zip' => $line[7]
      );

      $geocode = $getMap->geoLocate($location);

      $user_id = wp_insert_user( $userdata );

      foreach( $location as $key => $val ) {
        update_user_meta($user_id, '_' . $key, $val);
      }
      update_user_meta($user_id, '_lat', $geocode['lat']);
      update_user_meta($user_id, '_lng', $geocode['lng']);
    }
    fclose($f);
  }
  else {
    // error
  }
}

function m4h_bulk_import() {
  ?>
  <div class="wrap">
    <h2>M4H Bulk Add Users</h2>
    <form method="post" enctype="multipart/form-data">
      <table class="form-table">
        <tr>
          <th><label for="m4h-csv-import">Import CSV: </label></th>
          <td><input type="file" name="m4h-csv-import" id="m4h-csv-import" /></td>
        </tr>
        <tr>
          <th><label for="csv-header">CSV has headers: </label></th>
          <td><input type="checkbox" name="csv-header" id="csv-header" value="true" /></td>
        </tr>
      </table>
      <input type="submit" name="m4h-bulk-insert" id="m4h-bulk-insert" value="Upload CSV" />
    </form>
  </div><!-- /.wrap -->
  <?php
}

?>
